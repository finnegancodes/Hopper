#  Hopper

Hopper is an iOS tool that allows you to extract frames from any video file and convert them to a GIF. Pretty simple — for the user, not for me. Having to deal with ancient frameworks like `AVFoundation` was a huge pain.

There are also some bugs. It seems like if you try to convert a video file that is too large (either by length or it uses some super high quality encoding) the app will crash due to high memory usage. I'm sure there is a way around this, but my iOS skills just aren't there yet.

Apart from that though, it works great!

## Screenshots

![Hopper](https://gitlab.com/finnegancodes/Hopper/-/raw/main/Screenshots/hopper.png)

Made with Swift and SwiftUI.
