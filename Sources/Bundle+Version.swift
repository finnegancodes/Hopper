//
//  Bundle+Version.swift
//  Hopper
//
//  Created by Finnegan on 07/06/2024.
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String {
        return (infoDictionary?["CFBundleShortVersionString"] as? String) ?? "0"
    }
    var buildVersionNumber: String {
        return (infoDictionary?["CFBundleVersion"] as? String) ?? "0"
    }
}
