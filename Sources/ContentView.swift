//
//  ContentView.swift
//  Hopper
//
//  Created by Adam Oravec on 28/04/2024.
//

import SwiftUI
import PhotosUI

struct ContentView: View {
    
    @State private var path: [PhotosPickerItem] = []
    
    var body: some View {
        NavigationStack(path: $path) {
            WelcomeScreen(selectedVideo: Binding {
                path.first
            } set: { video in
                if let video {
                    path.append(video)
                } else {
                    path.removeAll()
                }
            })
            .navigationDestination(for: PhotosPickerItem.self) { video in
                ConverterScreen(video: video)
            }
        }
    }
}

#Preview {
    ContentView()
}
