//
//  ConverterScreen.swift
//  Hopper
//
//  Created by Adam Oravec on 29/04/2024.
//

import SwiftUI
import PhotosUI

struct ConverterScreen: View {
    
    @State private var viewModel: ViewModel
    
    init(video: PhotosPickerItem) {
        _viewModel = State(initialValue: ViewModel(video))
    }
    
    var body: some View {
        VStack {
            switch viewModel.status {
            case .created:
                GIFPropsForm(props: $viewModel.props) {
                    viewModel.convert()
                }
            case .preparing:
                ProgressView {
                    Text("Importing...")
                }
                .padding()
            case .converting(let frame, let totalFrames):
                ProgressView("Converting (\(frame)/\(totalFrames))...", value: Double(frame), total: Double(totalFrames))
                    .padding()
            case .converted(let url):
                ResultScreen(url: url) {
                    viewModel.reset()
                }
            case .failed(let error):
                VStack(spacing: 20) {
                    Spacer()
                    ContentUnavailableView("Something went wrong", systemImage: "exclamationmark.triangle", description: Text(error.description))
                    Spacer()
                    Button {
                        viewModel.reset()
                    } label: {
                        Label("Try Again", systemImage: "arrow.clockwise")
                    }
                    .buttonStyle(.bordered)
                    Spacer()
                }
            }
        }
        .onDisappear {
            GIF.clearTemporaryFiles()
        }
    }
}

extension ConverterScreen {
    @Observable
    class ViewModel: GIF.Delegate {
        var videoItem: PhotosPickerItem
        var status = GIF.Status.created
        var props = GIFProps()
        
        init(_ video: PhotosPickerItem) {
            self.videoItem = video
        }
        
        @MainActor
        func importVideo() async {
            let videoType = videoItem.supportedContentTypes[0]
            guard videoType == UTType.mpeg4Movie || videoType == UTType.quickTimeMovie else {
                self.status = .failed(GIFError("Invalid file format."))
                return
            }
            
            self.status = .preparing
            if let video = try? await videoItem.loadTransferable(type: Data.self) {
                let result = await GIF.make(from: video, videoType: videoType, props: props, delegate: self)
                switch result {
                case .success(let url):
                    self.status = .converted(url)
                case .failure(let error):
                    print(error.description)
                    self.status = .failed(error)
                }
            } else {
                self.status = .failed(GIFError("Unable to load file."))
            }
        }
        
        func convert() {
            guard props.isValid else { return }
            Task {
                await importVideo()
            }
        }
        
        func reset() {
            status = .created
            GIF.clearTemporaryFiles()
        }
    }
}
