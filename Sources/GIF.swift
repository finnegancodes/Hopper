//
//  GIF.swift
//  Hopper
//
//  Created by Adam Oravec on 29/04/2024.
//

import SwiftUI
import UniformTypeIdentifiers
import AVFoundation

@Observable
class GIF {
    private init() {}
    
    @MainActor
    static func make(from video: Data, videoType: UTType, props: GIFProps, delegate: Delegate? = nil) async -> Result<URL, GIFError> {
        let videoURL: URL
        do {
            videoURL = try Self.makeTempFile(video, videoType)
        } catch {
            return .failure(GIFError("Unable to create temporary file: \(error.localizedDescription)"))
        }
        let asset = AVAsset(url: videoURL)
        
        let frameGenerator = AVAssetImageGenerator(asset: asset)
        frameGenerator.requestedTimeToleranceBefore = CMTime(seconds: 0.05, preferredTimescale: 600)
        frameGenerator.requestedTimeToleranceAfter = CMTime(seconds: 0.05, preferredTimescale: 600)
        frameGenerator.appliesPreferredTrackTransform = true
        
        let duration: CMTime
        do {
            duration = try await asset.load(.duration)
        } catch {
            return .failure(GIFError("Unable to load video: \(error.localizedDescription)"))
        }
        let frames = Int(duration.seconds * props.fps)
        
        var timestamps = [NSValue]()
        for i in 0..<frames {
            let timestamp = (duration.seconds / Double(frames)) * Double(i)
            timestamps.append(NSValue(time: CMTime(seconds: timestamp, preferredTimescale: 600)))
        }
        
        for timestamp in timestamps {
            if !timestamp.timeValue.isValid {
                return .failure(GIFError("Unable to generate timestamps. Try lowering GIF frame rate."))
            }
        }
        
        let filename = UUID().uuidString
        let gif = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(filename, conformingTo: .gif)
        let gifDictionary = [kCGImagePropertyGIFDictionary: [
            kCGImagePropertyGIFLoopCount: 0
        ]]
        guard let file = CGImageDestinationCreateWithURL(gif as CFURL, UTType.gif.identifier as CFString, frames, nil) else {
            return .failure(GIFError("Unable to create GIF file."))
        }
        CGImageDestinationSetProperties(file, gifDictionary as CFDictionary)
        
        let frameDictionary = [
            kCGImagePropertyGIFDictionary: [
                kCGImagePropertyGIFDelayTime: 1 / props.fps,
                kCGImagePropertyGIFHasGlobalColorMap: false
            ]
        ]
        
        delegate?.status = .converting(frame: 0, totalFrames: frames)
        return await withCheckedContinuation { configurarion in
            var frameCount = 0
            frameGenerator.generateCGImagesAsynchronously(forTimes: timestamps) { requestedTime, frameImage, actualTime, result, error in
                // print("Converting frame \(frameCount) for time \(actualTime.seconds)")
                if let error {
                    print("Unable to generate image:", error.localizedDescription)
                } else if let frameImage {
                    CGImageDestinationAddImage(file, frameImage, frameDictionary as CFDictionary)
                }
                if frameCount >= frames - 1 {
                    guard CGImageDestinationFinalize(file) else {
                        print("Unable to finalize GIF")
                        configurarion.resume(returning: .failure(GIFError("Unable to finalize GIF.")))
                        return
                    }
                    Self.deleteTempFile(videoURL)
                    configurarion.resume(returning: .success(gif))
                    return
                }
                frameCount += 1
                delegate?.status = .converting(frame: frameCount, totalFrames: frames)
            }
        }
    }
    
    private static func makeTempFile(_ video: Data, _ type: UTType) throws -> URL {
        let tempFile = URL(filePath: NSTemporaryDirectory()).appendingPathComponent("temp", conformingTo: type)
        try video.write(to: tempFile, options: [.atomic, .completeFileProtection])
        return tempFile
    }
    
    private static func deleteTempFile(_ url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print("Failed to delete temporary file")
        }
    }
    
    static func clearTemporaryFiles() {
        guard let items = try? FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory()) else { return }
        for item in items {
            let url = URL(filePath: NSTemporaryDirectory()).appendingPathComponent(item, conformingTo: .fileURL)
            do {
                try FileManager.default.removeItem(at: url)
            } catch {
                print("Failed to delete file: \(item)", error.localizedDescription)
            }
        }
    }
}

extension GIF {
    enum Status {
        case preparing
        case converting(frame: Int, totalFrames: Int)
        case converted(URL)
        case created
        case failed(GIFError)
    }
}

extension GIF {
    protocol Delegate: AnyObject {
        var status: Status { get set }
    }
}

struct GIFError: Error, CustomStringConvertible {
    var description: String
    
    init(_ description: String) {
        self.description = description
    }
}
