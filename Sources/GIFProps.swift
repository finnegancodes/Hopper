//
//  GIFProps.swift
//  Hopper
//
//  Created by Adam Oravec on 29/04/2024.
//

import Foundation

struct GIFProps {
    var fps = 15.0
    
    var isValid: Bool {
        fps > 0 && fps <= 30
    }
}
