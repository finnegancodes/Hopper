//
//  GIFPropsForm.swift
//  Hopper
//
//  Created by Adam Oravec on 29/04/2024.
//

import SwiftUI

struct GIFPropsForm: View {
    
    @Binding var props: GIFProps
    let onConvert: () -> Void
    
    var body: some View {
        ScrollView(.vertical) {
            VStack(alignment: .leading, spacing: 16) {
                LabeledContent("FPS") {
                    TextField("0", value: $props.fps, format: .number)
                        .textFieldStyle(.roundedBorder)
                }
                HStack {
                    Spacer()
                    Button("Convert") {
                        onConvert()
                    }
                    .buttonStyle(.borderedProminent)
                    .disabled(!props.isValid)
                    Spacer()
                }
            }
            .multilineTextAlignment(.leading)
            .padding()
        }
        .navigationTitle("GIF Options")
        .navigationBarTitleDisplayMode(.inline)
    }
}

extension GIFPropsForm {
    static let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.maximumFractionDigits = 5
        nf.decimalSeparator = "."
        return nf
    }()
}

#Preview {
    GIFPropsForm(props: .constant(.init())) {}
}
