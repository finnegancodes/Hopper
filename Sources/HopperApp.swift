//
//  HopperApp.swift
//  Hopper
//
//  Created by Adam Oravec on 28/04/2024.
//

import SwiftUI

@main
struct HopperApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
