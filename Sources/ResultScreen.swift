//
//  ResultScreen.swift
//  Hopper
//
//  Created by Adam Oravec on 29/04/2024.
//

import SwiftUI

struct ResultScreen: View {
    
    let url: URL
    let onReset: () -> Void
    
    var body: some View {
        VStack(spacing: 8) {
            Spacer()
            Text("Done!")
                .font(.title.bold())
            Text("Save the GIF by pressing 'Share'")
                .font(.subheadline)
                .foregroundStyle(.secondary)
            Spacer()
            Spacer()
            ShareLink(item: url)
            Spacer()
        }
    }
}
