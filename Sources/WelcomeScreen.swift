//
//  WelcomeScreen.swift
//  Hopper
//
//  Created by Adam Oravec on 29/04/2024.
//

import SwiftUI
import PhotosUI

struct WelcomeScreen: View {
    
    @Binding var selectedVideo: PhotosPickerItem?
    
    var body: some View {
        VStack(spacing: 8) {
            Spacer()
            Text("Hopper")
                .font(.largeTitle.bold())
            Text("Convert videos to GIFs.")
                .font(.subheadline)
                .foregroundStyle(.secondary)
            Spacer()
            Spacer()
            PhotosPicker("Choose Video", selection: $selectedVideo, matching: .videos)
                .buttonStyle(.borderedProminent)
            Spacer()
            Text("v\(Bundle.main.releaseVersionNumber) (\(Bundle.main.buildVersionNumber))")
                .foregroundStyle(.secondary)
                .font(.subheadline)
        }
        .padding(.vertical, 10)
    }
}
